#ifndef TIMERCONFIG_H
#define TIMERCONFIG_H

#include <QTime>
#include <QFont>
#include <QLinearGradient>
#include <QFile>

class Config
{
public:
    enum class TimerType
    {
        Undef,
        CountDown = Undef,
        CountUp
    };

    enum class BackgroundStyle
    {
        Undef,
        ColorOverTime = Undef,
        ColorStatic,
//        ImageFile
    };

public:
    explicit Config();

    TimerType getType() const;
    void setType(const TimerType &value);

    QTime getStartTime() const;
    void setStartTime(const QTime &value);

    QTime getStopTime() const;
    void setStopTime(const QTime &value);

    QFont getTimerFont() const;
    void setTimerFont(const QFont &value);

    bool getContinueTimerAfterEnd() const;
    void setContinueTimerAfterEnd(bool value);

    bool getFullScreen() const;
    void setFullScreen(bool value);

    BackgroundStyle getBackground() const;
    void setBackground(const BackgroundStyle &value);

    QLinearGradient getBackgroundColorOverTime() const;
    void setBackgroundColorOverTime(const QLinearGradient &value);

    QColor getBackgroundColorStatic() const;
    void setBackgroundColorStatic(const QColor &value);

    bool getHideTimerAtEnd() const;
    void setHideTimerAtEnd(bool value);

    QString getShowTextAtEnd() const;
    void setShowTextAtEnd(const QString &value);

private:
    void setDefaults();
    // TODO: we need save/restore functionality

    // The timer configuraion
    TimerType type;
    QTime startTime;
    QTime stopTime;
    bool continueTimerAfterEnd;

    // Ui general configuration
    bool fullScreen;
    QFont timerFont;

    // Ui background configuration
    BackgroundStyle background;
    QLinearGradient backgroundColorOverTime;    // TODO: maybe better?
    QColor backgroundColorStatic;
//    QFile backgroundImageFile;

    // Ui behaviour after timer ends
    bool hideTimerAtEnd;
    QString showTextAtEnd;
//    QFile imageFileAtEnd;
//    bool flashBackground;
};

#endif // TIMERCONFIG_H
