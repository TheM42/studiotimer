#ifndef STUDIOTIMER_H
#define STUDIOTIMER_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class StudioTimer; }
QT_END_NAMESPACE

class StudioTimer : public QMainWindow
{
    Q_OBJECT

public:
    StudioTimer(QWidget *parent = nullptr);
    ~StudioTimer();

protected:
    bool eventFilter(QObject *obj, QEvent *ev) override;

private slots:
    void setTime(QTime &currentTime);
    void setRunning(bool running);

    void helpClicked();
    void configClicked();

private:
    Ui::StudioTimer *ui;
};
#endif // STUDIOTIMER_H
