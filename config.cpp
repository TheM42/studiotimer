#include "config.h"

#include <QDebug>

Config::Config()
{
    setDefaults();
}

void Config::setDefaults()
{
    qDebug() << "Config::setDefaults()";

    /*
     * These are the defaults we need for my hobbyist recording set.
     * Feel free to set your own defaults here!
     */
    type = TimerType::CountDown;
    startTime = QTime(0,5,25);
    stopTime = QTime(0,0,0);
    timerFont = QFont();    // Application default font
    continueTimerAfterEnd = false;

    fullScreen = true;

    background = BackgroundStyle::ColorOverTime;
    backgroundColorOverTime = QLinearGradient(QPointF(0,0), QPointF(1,0));
    backgroundColorOverTime.setColorAt(0, Qt::green);
    backgroundColorOverTime.setColorAt(1, Qt::red);
    backgroundColorStatic = QColor(Qt::blue);
//    backgroundImageFile = QFile();

    hideTimerAtEnd = true;
    showTextAtEnd = QString("Bitte weiter reden!");
//    imageFileAtEnd = QFile();
//    flashBackground = false;
}

QString Config::getShowTextAtEnd() const
{
    return showTextAtEnd;
}

void Config::setShowTextAtEnd(const QString &value)
{
    showTextAtEnd = value;
}

bool Config::getHideTimerAtEnd() const
{
    return hideTimerAtEnd;
}

void Config::setHideTimerAtEnd(bool value)
{
    hideTimerAtEnd = value;
}

QColor Config::getBackgroundColorStatic() const
{
    return backgroundColorStatic;
}

void Config::setBackgroundColorStatic(const QColor &value)
{
    backgroundColorStatic = value;
}

QLinearGradient Config::getBackgroundColorOverTime() const
{
    return backgroundColorOverTime;
}

void Config::setBackgroundColorOverTime(const QLinearGradient &value)
{
    backgroundColorOverTime = value;
}

Config::BackgroundStyle Config::getBackground() const
{
    return background;
}

void Config::setBackground(const BackgroundStyle &value)
{
    background = value;
}

bool Config::getFullScreen() const
{
    return fullScreen;
}

void Config::setFullScreen(bool value)
{
    fullScreen = value;
}

bool Config::getContinueTimerAfterEnd() const
{
    return continueTimerAfterEnd;
}

void Config::setContinueTimerAfterEnd(bool value)
{
    continueTimerAfterEnd = value;
}

QFont Config::getTimerFont() const
{
    return timerFont;
}

void Config::setTimerFont(const QFont &value)
{
    timerFont = value;
}

QTime Config::getStopTime() const
{
    return stopTime;
}

void Config::setStopTime(const QTime &value)
{
    stopTime = value;
}

QTime Config::getStartTime() const
{
    return startTime;
}

void Config::setStartTime(const QTime &value)
{
    startTime = value;
}

Config::TimerType Config::getType() const
{
    return type;
}

void Config::setType(const TimerType &value)
{
    type = value;
}
