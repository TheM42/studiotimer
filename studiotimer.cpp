#include "studiotimer.h"
#include "ui_studiotimer.h"
#include "config.h"
#include "timer.h"

#include <QStyle>
#include <QKeyEvent>
#include <QDebug>
#include <QMessageBox>

StudioTimer::StudioTimer(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::StudioTimer)
{
    // Create logic
    Config configuration;
    Timer* timer = new Timer(this);

    // Setup UI
    ui->setupUi(this);
    ui->PlayPause->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->SeekBack->setIcon(style()->standardIcon(QStyle::SP_MediaSeekBackward));
    ui->SeekForward->setIcon(style()->standardIcon(QStyle::SP_MediaSeekForward));
    ui->SkipBackwards->setIcon(style()->standardIcon(QStyle::SP_MediaSkipBackward));
    ui->SkipForkwards->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));
    ui->Config->setIcon(style()->standardIcon(QStyle::SP_MessageBoxInformation)); // TODO: better icon
    ui->Help->setIcon(style()->standardIcon(QStyle::SP_MessageBoxQuestion));

    // Setup key bindings
    this->installEventFilter(this);

    // Connect UI to Logic
    connect(ui->PlayPause, SIGNAL(clicked()), timer, SLOT(startPauseTimer()));
    connect(ui->SeekBack, SIGNAL(clicked()), timer, SLOT(seekBackward()));
    connect(ui->SeekForward, SIGNAL(clicked()), timer, SLOT(seekForeward()));
    connect(ui->SkipBackwards, SIGNAL(clicked()), timer, SLOT(skipBackward()));
    connect(ui->SkipForkwards, SIGNAL(clicked()), timer, SLOT(skipForeward()));
    connect(ui->Config, SIGNAL(clicked()), this, SLOT(configClicked()));
    connect(ui->Help, SIGNAL(clicked()), this, SLOT(helpClicked()));

    // Connect logic to UI
    connect(timer, SIGNAL(timerChanged(QTime&)), this, SLOT(setTime(QTime&)));
    connect(timer, SIGNAL(timerRunning(bool)), this, SLOT(setRunning(bool)));

    // Load and set configuration
    timer->setConfig(configuration);
    // TODO: load and runtime change management
}

StudioTimer::~StudioTimer()
{
    delete ui;
}

/*
 * Grab all key press events globally reserved for timer control!
 * Prevent key focus on doing stupid things;-)
 */
bool StudioTimer::eventFilter(QObject *obj, QEvent *ev)
{
    if (ev->type() == QEvent::KeyPress)
    {
        const QKeyEvent *keyEvent = static_cast<QKeyEvent*>(ev);
        if(keyEvent)
        {
            /*
             * Map reserved keys to buttons!
             */
            switch(keyEvent->key())
            {
                case Qt::Key::Key_Home:
                    ui->SkipBackwards->click();
                    break;
                case Qt::Key::Key_End:
                    ui->SkipForkwards->click();
                    break;
                case Qt::Key::Key_Left:
                case Qt::Key::Key_PageUp:
                    ui->SeekBack->click();
                    break;
                case Qt::Key::Key_Right:
                case Qt::Key::Key_PageDown:
                    ui->SeekForward->click();
                    break;
                case Qt::Key::Key_Space:
                case Qt::Key::Key_Enter:
                    ui->PlayPause->click();
                    break;
                case Qt::Key::Key_F1:
                case Qt::Key::Key_Help:
                    ui->Help->click();
                    break;
                case Qt::Key::Key_Settings:
                    ui->Config->click();
                    break;
                default:
                    // Ignore all others
                    return false;
                    break;
            }
            qDebug() << "Catched reserved key event!";
            return true;
        }
        else
        {
            qDebug() << "static_cast QKeyEvent failed!";
        }
    }
    return false;
}

void StudioTimer::setTime(QTime& currentTime)
{
    if(currentTime.isValid())
    {
        ui->Time->setText(currentTime.toString("hh:mm:ss"));
    }
    else
    {
        ui->Time->setText("--:--;--");
    }
}

void StudioTimer::setRunning(bool running)
{
    ui->PlayPause->setIcon(running ? style()->standardIcon(QStyle::SP_MediaPause) : style()->standardIcon(QStyle::SP_MediaPlay));
    // TODO: hide controls when running
}

void StudioTimer::helpClicked()
{
    QMessageBox::warning(
        this,
        tr("Sorry Bro..."),
                tr("...help dialog is comming soon! Keep calm and continue timing...!") );
}

void StudioTimer::configClicked()
{
    QMessageBox::warning(
        this,
        tr("Move along!"),
        tr("This is not the dialog you are looking for!") );
}
