#ifndef TIMER_H
#define TIMER_H

#include "config.h"

#include <QObject>
#include <QTimer>
#include <QTime>

class Timer : public QObject
{
    Q_OBJECT

public:
    explicit Timer(QObject *parent = nullptr);

signals:
    void timerChanged(QTime& value);
    void timerRunning(bool running);

public slots:
    void setConfig(Config config);
    void startPauseTimer();
    void seekForeward();
    void seekBackward();
    void skipForeward();
    void skipBackward();

private slots:
    void onTimerTimeout();

private:
    void addMsecToCurrentTime(unsigned int msec);
    void reduceMsecFromCurrentTime(unsigned int msec);
    bool endReachedOrExceeded() const;

    QTimer* m_timer;
    Config m_config;

    QTime m_currentTime;

    const unsigned int m_seekTimeMSec;
};

#endif // TIMER_H
