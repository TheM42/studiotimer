#include "timer.h"

#include <QDebug>

Timer::Timer(QObject *parent) : QObject(parent), m_seekTimeMSec(1000)
{
    m_timer = new QTimer(this);
    m_timer->setSingleShot(false);
    m_timer->setInterval(1000);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(onTimerTimeout()) );

    m_currentTime = QTime();
}

void Timer::setConfig(Config config)
{
    qDebug() << "Set config: Type:" << (int)config.getType() << "Start time:" << config.getStartTime() << "Stop time" << config.getStopTime() << "Continue end:" << config.getContinueTimerAfterEnd();

    // Reset timer
    m_timer->stop();
    emit timerRunning(m_timer->isActive());
    m_currentTime = QTime();
    emit timerChanged(m_currentTime);

    // Store a copy(!) of the configuration (that can be locally changed!)
    m_config = config;

    // Sanitize check the configuration
    if(m_config.getType() == Config::TimerType::CountDown)
    {
        // Start must be larger then stop, i.e. 05:30 > 00:00
        if(m_config.getStartTime() <= m_config.getStopTime())
        {
            qDebug() << "Timer config error: Start must be larger then stop time! Start time:" << config.getStartTime() << "Stop time" << config.getStopTime();
            m_config.setStopTime(QTime(0,0,0));
            qDebug() << "Sanitized timer to: Start time:" << config.getStartTime() << "Stop time" << config.getStopTime();
        }
    }
    else // Count up
    {
        // Start must be smaller then stop, i.e. 00:00 < 05:30
        if(m_config.getStartTime() >= m_config.getStopTime())
        {
            qDebug() << "Timer config error: Start must be smaller then stop time! Start time:" << config.getStartTime() << "Stop time" << config.getStopTime();
            m_config.setStartTime(QTime(0,0,0));
            qDebug() << "Sanitized timer to: Start time:" << config.getStartTime() << "Stop time" << config.getStopTime();
        }
    }    

    // Set the time that is been worked on
    m_currentTime = m_config.getStartTime();
    emit timerChanged(m_currentTime);
}

void Timer::startPauseTimer()
{
    // Toggle between running and paused
    if(m_timer->isActive())
    {
        qDebug() << "stop timer";
        m_timer->stop();
    }
    else
    {
        if(m_config.getType() == Config::TimerType::CountUp)
        {
            if(m_currentTime >= m_config.getStopTime() && !m_config.getContinueTimerAfterEnd())
            {
                qDebug() << "ignore start timer, current time >= stopTime (count up)";
            }
            else
            {
                qDebug() << "start timer";
                m_timer->start();
            }
        }
        else /* count down */
        {
            if(m_currentTime <= m_config.getStopTime() && !m_config.getContinueTimerAfterEnd())
            {
                qDebug() << "ignore start timer, current time <= stopTime (count down)";
            }
            else
            {
                qDebug() << "start timer";
                m_timer->start();
            }
        }
    }
    emit timerRunning(m_timer->isActive());
}

void Timer::seekForeward()
{
    // Seek one second forewards relative to counter direction!
    if(m_config.getType() == Config::TimerType::CountUp)
    {
        addMsecToCurrentTime(m_seekTimeMSec);
    }
    else // Count down
    {
        reduceMsecFromCurrentTime(m_seekTimeMSec);
    }
}

void Timer::seekBackward()
{
    // Seek one second backwards relative to counter direction
    if(m_config.getType() == Config::TimerType::CountUp)
    {
        reduceMsecFromCurrentTime(m_seekTimeMSec);
    }
    else // Count down
    {
        addMsecToCurrentTime(m_seekTimeMSec);
    }
}

void Timer::skipForeward()
{
    // Skip to the end of the timer
    m_currentTime = m_config.getStopTime();
    emit timerChanged(m_currentTime);
}

void Timer::skipBackward()
{
    // Skip to the beginning of the timer
    m_currentTime = m_config.getStartTime();
    emit timerChanged(m_currentTime);
}

void Timer::onTimerTimeout()
{
    // Update current time, will be called in timer intervall
    if(m_config.getType() == Config::TimerType::CountUp)
    {
        addMsecToCurrentTime(m_timer->interval());
    }
    else // Count down
    {
        reduceMsecFromCurrentTime(m_timer->interval());
    }
    emit timerChanged(m_currentTime);

    // Continue after end?
    if(endReachedOrExceeded())
    {
        if(!m_config.getContinueTimerAfterEnd())
        {
            // stop the timer
            qDebug() << "stop timer at end";
            m_timer->stop();
            emit timerRunning(m_timer->isActive());
        }
    }
}

void Timer::addMsecToCurrentTime(unsigned int msec)
{
    // Count up, prevent positive wrapping
    const QTime newTime = m_currentTime.addMSecs(msec);
    if(newTime < m_currentTime)
    {
        // Time wrapped at 24h ceil at max
        m_currentTime.setHMS(23,59,59,999);
    }
    else
    {
        m_currentTime = newTime;
    }
    emit timerChanged(m_currentTime);
}

void Timer::reduceMsecFromCurrentTime(unsigned int msec)
{
    // Count down, prevent negative wrapping
    const QTime newTime = m_currentTime.addMSecs((-1)*msec);
    if(newTime > m_currentTime)
    {
        // Time wrapped at 00h floor at min
        m_currentTime.setHMS(0,0,0,0);
    }
    else
    {
        m_currentTime = newTime;
    }
    emit timerChanged(m_currentTime);
}

bool Timer::endReachedOrExceeded() const
{
    if(m_config.getType() == Config::TimerType::CountUp)
    {
        return m_currentTime >= m_config.getStopTime();
    }
    else // Count down
    {
        return m_currentTime <= m_config.getStopTime();
    }
}
