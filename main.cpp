#include "studiotimer.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    StudioTimer w;
    w.show();
    return a.exec();
}
